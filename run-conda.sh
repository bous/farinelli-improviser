#!/usr/bin/env bash
env=farinelli-improviser
root="$(cd "$(dirname "${BASH_SOURCE[0]}" )" && pwd)"

eval "$(conda shell.bash hook)"
conda activate $env

"$root/sing.py"
