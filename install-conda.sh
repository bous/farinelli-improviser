#!/usr/bin/env bash

env=farinelli-improviser

pkgs="python=3.9\
      librosa\
      numpy\
      PyYAML\
      python-sounddevice"

conda create \
   -y \
   -n $env \
   -c conda-forge \
   $pkgs

conda activate $env
pip install -y fuzzylogic
