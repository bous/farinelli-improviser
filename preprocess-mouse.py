#!/usr/bin/env python
from __future__ import annotations

from pathlib import Path

import numpy as np
from matplotlib import pyplot as plt
from scipy import signal

mouse_recording = Path.home() / "tmp" / "mouse3.npy"


def main():

    rec = np.load(mouse_recording)

    recs = np.stack(rec[k : -10 + k] for k in range(10))
    splt = split_by_zero(rec)
    print([len(x) * 0.05 for x in splt])

    peak = np.max(recs, axis=0)
    act = np.mean(recs, axis=0)

    avr = signal.lfilter([0.02], [1, -0.98], act)
    t = np.arange(len(peak)) * 0.050

    fig, ax = plt.subplots()
    ax.plot(t, peak, label="peak")
    ax.plot(t, act, label="act")
    ax.plot(np.arange(len(avr)) * 0.050, avr, label="avr")
    ax.legend()

    fig, ax = plt.subplots()
    for x, label in [(peak, "peak"), (act, "activity"), (avr, "average")]:
        vs = np.arange(1000)[np.newaxis, :]
        # r0 = x[x > 0]
        r0 = x
        ds = r0[:, np.newaxis] < vs
        d = np.sum(ds, axis=0) / len(r0)
        ax.plot(d, label=label)
        # ax.plot(t, rec)
    ax.legend()
    ax.grid(which="both")

    plt.show()


def split_by_zero(a):
    a = signal.lfilter([0.01], [1, -0.99], a)
    idx = np.where(a < 0.1)[0]
    splt = np.split(a[idx], np.where(np.diff(idx) != 1)[0] + 1)
    return [x for x in splt if len(x) > 400]


if __name__ == "__main__":
    main()
