from __future__ import annotations

from dataclasses import dataclass, field
from enum import Enum
from subprocess import PIPE, Popen
from typing import Optional, Sequence

import numpy as np


class Format(Enum):
    FLOAT_LE = "FLOAT_LE"
    S16_LE = "S16_LE"
    S32_LE = "S32_LE"


@dataclass
class APlay:
    rate: float
    format: Format = Format.FLOAT_LE
    channels: int = 1
    aplay_process: Optional[Popen] = field(init=False, default=None)

    def __enter__(self) -> APlay:
        self.open()
        return self

    def __exit__(self, *args, **kwargs):
        self.close()

    def open(self):
        self.aplay_process = Popen(self.command, stdin=PIPE)

    def close(self):
        if self.aplay_process is not None:
            self.aplay_process.terminate()
            self.aplay_process = None

    def play(self, audio_data: np.ndarray):
        assert self.aplay_process is not None, "aplay is not running!"
        assert (
            self.aplay_process.stdin is not None
        ), "aplay process has no stdin!"
        self.aplay_process.stdin.write(numpy_to_bytes(audio_data, self.format))

    @property
    def command(self) -> Sequence[str]:
        return [
            "aplay",
            "-r",
            f"{self.rate}",
            "-f",
            self.format.value,
            "-c",
            f"{self.channels}",
        ]


def numpy_to_bytes(data: np.ndarray, format: Format) -> bytes:
    if format == Format.FLOAT_LE:
        return data.astype(np.float32).tobytes()
    if format == Format.S16_LE:
        return (data * 2 ** 15).astype(np.int16).tobytes()
    if format == Format.S32_LE:
        return (data * 2 ** 31).astype(np.int32).tobytes()
