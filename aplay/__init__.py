import sys

if sys.platform.lower() == "linux":
    from aplay.aplay import APlay
else:
    from aplay.sdadapter import SounddeviceAdapter as APlay


__all__ = ["APlay"]
