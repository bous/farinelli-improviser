from __future__ import annotations

from dataclasses import dataclass, field

import numpy as np
import sounddevice as sd

from aplay.aplay import Format

sd.default.channels = 1
sd.default.samplerate = 24000


@dataclass
class SounddeviceAdapter:
    """This is used in non linux environments where aplay is not installed."""

    rate: float
    format: Format = Format.FLOAT_LE
    channels: int = 1
    stream: sd.OutputStream = field(
        init=False, default_factory=sd.OutputStream
    )

    def __enter__(self) -> SounddeviceAdapter:
        self.stream = sd.OutputStream()
        self.stream.start()
        return self

    def __exit__(self, *args, **kwargs):
        self.stream.stop()

    def play(self, audio_data: np.ndarray):
        self.stream.write(audio_data.astype("f"))
