#!/usr/bin/env python
from __future__ import annotations

import glob
from pathlib import Path
from queue import Queue
from time import sleep
from typing import Tuple

import librosa
import numpy as np

from improviser.activity import Activity, ActivityReader, ActivityRecording
from improviser.display import Display
from improviser.markers import open_connections, open_markers
from improviser.playback import Playback
from improviser.statemachine import Fragment, Improviser, Key, Rest

markerfile = Path(__file__).parent / "data" / "segments.txt"
connectionsfile = Path(__file__).parent / "data" / "connections.csv"
mouse_recording = Path(__file__).parent / "data" / "mouse.npy"
# connectionsfile = (
#     Path("/")
#     / "data2"
#     / "anasynth_nonbp"
#     / "bous"
#     / "Downloads"
#     / "Connections Smooth_Rough - Sheet1(*).csv"
# )
audiofiles = (
    # Path(__file__).parent / "audio" / "E.wav",
    # Path(__file__).parent / "audio" / "F.wav",
    Path(__file__).parent / "audio" / "Gb.wav",
    Path(__file__).parent / "audio" / "G.wav",
    Path(__file__).parent / "audio" / "Ab.wav",
    Path(__file__).parent / "audio" / "A.wav",
    Path(__file__).parent / "audio" / "Bb.wav",
    Path(__file__).parent / "audio" / "B.wav",
    Path(__file__).parent / "audio" / "C.wav",
    Path(__file__).parent / "audio" / "Db.wav",
    Path(__file__).parent / "audio" / "D.wav",
)

# mouse_recording = Path.home() / "tmp" / "mouse.npy"


def main(record: str = "", mouse_recording: str | Path = mouse_recording):
    markers = open_markers(markerfile)

    cf = Path(sorted(glob.glob(str(connectionsfile)))[-1])
    print(cf)

    connections = open_connections(cf)
    audio_file = np.stack(
        [librosa.load(audiofile, sr=24000)[0] for audiofile in audiofiles],
        axis=-1,
    )

    display = Display()
    activity = Activity(display)

    if mouse_recording:
        activity_reader = ActivityRecording(activity, Path(mouse_recording))
    else:
        activity_reader = ActivityReader(activity)
    activity_reader.start()

    fragment_queue: Queue[Tuple[Fragment | Rest, Key]] = Queue(maxsize=1)

    improviser = Improviser(
        activity,
        fragment_queue,
        markers,
        connections,
        start=(
            Fragment.parse("Qa_i"),
            Key(5, audio_file.shape[-1], original=5),
        ),
    )
    improviser.start()

    if record:
        from pysndfile import sndio

    playback = Playback(
        audio_file, markers, fragment_queue, activity, record=bool(record)
    )
    playback.start()

    try:
        display.run()
    finally:
        activity_reader.stop()
        improviser.stop()
        playback.stop()
        while not fragment_queue.empty():
            fragment_queue.get_nowait()
        if record and playback.recording is not None:
            from pysndfile import sndio

            print(f"Writing recording to {record}")
            recording = np.concatenate(playback.recording, axis=0)
            sndio.write(record, recording, playback.samplerate, format="wav")


class NoDisplay:
    def update(self, activity: Activity):
        pass

    def run(self):
        while True:
            sleep(1.0)


if __name__ == "__main__":
    from sys import argv

    main(*argv[1:])
