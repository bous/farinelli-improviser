#!/usr/bin/env python

from __future__ import annotations

from math import sqrt
from pathlib import Path
from time import sleep
from typing import Tuple

import numpy as np
import pyautogui


def dist(xs: Tuple[float, ...], ys: Tuple[float, ...]) -> float:
    return sqrt(sum((x - y) ** 2 for x, y in zip(xs, ys)))


def main():
    speeds = []
    prev_pos = pyautogui.position()
    try:
        while True:
            mouse_pos = pyautogui.position()
            mouse_speed = dist(mouse_pos, prev_pos)
            speeds.append(mouse_speed)
            prev_pos = mouse_pos
            sleep(0.050)
    except KeyboardInterrupt:
        pass
    positions = np.array(speeds)
    np.save(Path.home() / "tmp" / "mouse", positions)


if __name__ == "__main__":
    main()
