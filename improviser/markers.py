from __future__ import annotations

from pathlib import Path
from typing import Dict, Iterator, Mapping, Sequence, Tuple

from improviser.statemachine import Connection, Fragment


def open_markers(path: Path) -> Mapping[Fragment, Tuple[float, float]]:
    lines = path.read_text().splitlines()
    markers = {}
    for line in lines[1:]:
        begin, end, name = line.split("\t")
        try:
            markers[Fragment.parse(name)] = float(begin), float(end)
        except (ValueError, IndexError):
            if begin != end:
                print(f"could not parse '{begin} {end} {name}'")
        except:
            breakpoint()
            raise
    return markers


def open_connections(path: Path) -> Mapping[Fragment, Connection]:
    csv = CSV(path)
    connections: Dict[Fragment, Connection] = {}
    csvit = iter(csv)
    next(csvit)
    for row in csvit:
        f, s, r, e, *_ = row
        smooth = s.split("\n")
        rough = r.split("\n")
        end = e.split("\n")
        try:
            connections[Fragment.parse(f)] = Connection(
                smooth=tuple(Fragment.parse(s) for s in smooth if s),
                rough=tuple(Fragment.parse(r) for r in rough if r),
                end=tuple(Fragment.parse(e) for e in end if e),
            )
        except Exception as e:
            print(f"Problem with {row}")
            # raise
    return connections


class CSV:
    def __init__(self, path: Path, sep: str = ","):
        self.data = path.read_text()
        self.sep = sep

    def __iter__(self) -> Iterator[Sequence[str]]:
        data = self.data
        items = []
        while data:
            item, data = data.split(self.sep, 1)
            if item.startswith('"'):
                item, rem = item[1:].split('"', 1)
                items.append(item)
                if rem and rem.startswith("\n"):
                    yield items
                    items = [rem[1:]]
                    continue
            else:
                if "\n" in item:
                    item, rem = item.split("\n", 1)
                    items.append(item)
                    yield items
                    items = [rem]
                    continue
                else:
                    items.append(item)
        if items:
            yield items
