from typing import Callable

from fuzzylogic.classes import Domain, Rule, Set
from fuzzylogic.functions import R, S, alpha, gauss, trapezoid


def singleton(p: float, width: float = 0.1) -> Callable[[float], float]:
    def f(x: float) -> float:
        if p - width < x < p + width:
            return 1
        return 0

    return f


activity = Domain("Activity", 0, 1000)
activity.none = S(0, 2)  # 0
activity.low = trapezoid(0, 2, 13, 23)  # 5
activity.mid = trapezoid(13, 23, 45, 85)  # 75
activity.high = R(45, 85)  # 500

average = Domain("Activity", 0, 1000)
average.none = S(0, 2)  # 0
average.low = trapezoid(0, 2, 28, 40)  # 5
average.mid = trapezoid(28, 40, 60, 80)  # 75
average.high = R(60, 80)  # 500

peak = Domain("Activity", 0, 1000)
peak.none = S(0, 2)  # 0
peak.low = trapezoid(0, 2, 32, 68)  # 5
peak.mid = trapezoid(32, 68, 145, 295)  # 75
peak.high = R(145, 295)  # 500

presence = Domain("Presence", 0, 300)
presence.none = R(45, 60)
presence.recent = trapezoid(0, 30, 45, 60)
presence.yes = S(0, 30)


jump_prob = Domain("jump_probability", 0, 1, res=0.01)
jump_prob.high = singleton(0.4)
jump_prob.low = singleton(0.03)
jump_rule = Rule(
    {
        (peak.none,): jump_prob.low,
        (peak.low,): jump_prob.low,
        (peak.mid,): jump_prob.high,
        (peak.high,): jump_prob.high,
    }
)

exit_prob = Domain("exit_probability", 0, 1, res=0.01)
exit_prob.low = singleton(0.05)
exit_prob.normal = singleton(0.2)
exit_prob.very_high = singleton(0.9)

exit_rule = Rule(
    {
        (
            presence.none,
            average.none,
        ): exit_prob.normal,
        (
            presence.none,
            average.low,
        ): exit_prob.low,
        (
            presence.none,
            average.mid,
        ): exit_prob.low,
        (
            presence.none,
            average.high,
        ): exit_prob.very_high,
        (
            presence.recent,
            average.none,
        ): exit_prob.normal,
        (
            presence.recent,
            average.low,
        ): exit_prob.normal,
        (
            presence.recent,
            average.mid,
        ): exit_prob.normal,
        (
            presence.recent,
            average.high,
        ): exit_prob.normal,
        (
            presence.yes,
            average.none,
        ): exit_prob.normal,
        (
            presence.yes,
            average.low,
        ): exit_prob.normal,
        (
            presence.yes,
            average.mid,
        ): exit_prob.normal,
        (
            presence.yes,
            average.high,
        ): exit_prob.normal,
    }
)

go_back_prob = Domain("go_back_probability", 0, 1, res=0.01)
go_back_prob.high = singleton(0.4)
go_back_prob.low = singleton(0)

go_back_rule = Rule(
    {
        (average.none,): go_back_prob.high,
        (average.low,): go_back_prob.low,
        (average.mid,): go_back_prob.low,
        (average.high,): go_back_prob.low,
    }
)


go_up_prob = Domain("go_up_probability", 0, 1, res=0.01)
go_up_prob.high = singleton(1)
go_up_prob.low = singleton(0.0)

go_up_rule = Rule(
    {
        (average.none,): go_up_prob.low,
        (average.low,): go_up_prob.low,
        (average.mid,): go_up_prob.high,
        (average.high,): go_up_prob.high,
    }
)

go_down_prob = Domain("go_down_probability", 0, 1, res=0.01)
go_down_prob.high = singleton(1)
go_down_prob.low = singleton(0.0)

go_down_rule = Rule(
    {
        (peak.none,): go_down_prob.low,
        (peak.low,): go_down_prob.high,
        (peak.mid,): go_down_prob.high,
        (peak.high,): go_down_prob.low,
    }
)

rest_duration = Domain("rest_duration", 0, 20, res=0.1)
rest_duration.long = singleton(5)
rest_duration.short = singleton(2)
rest_duration.none = singleton(0)

rest_rule = Rule(
    {
        (presence.yes,): rest_duration.none,
        (presence.recent,): rest_duration.short,
        (presence.none,): rest_duration.long,
    }
)
