from __future__ import annotations

import math
import random
from collections import deque
from dataclasses import dataclass, replace
from enum import Enum
from itertools import chain
from queue import Queue
from threading import Thread
from typing import Any, Deque, Iterable, List, Mapping, Sequence, Tuple

from improviser.activity import Activity

romans = [None, "i", "ii", "iii", "iv", "v", "vi", "vii", "viii", "ix", "x"]


@dataclass(frozen=True)
class Rest:
    duration: float


class Phrase(Enum):
    Q = "Q"
    C = "C"
    S = "S"
    F = "F"


class Energy(Enum):
    a = "a"
    b = "b"
    c = "c"
    d = "d"
    alfa = "alfa"
    beta = "beta"
    gamma = "gamma"
    delta = "delta"

    @classmethod
    def parse(cls, string):
        try:
            return cls(string)
        except ValueError:
            return {
                "alfa": cls.b,
                "beta": cls.d,
                "gamma": cls.c,
                "delta": cls.a,
            }[string]


class Trio(Enum):
    W = "W"
    X = "X"
    Y = "Y"
    Z = "Z"


@dataclass(frozen=True)
class Part:
    value: str

    # value: int
    # variation: bool

    def __str__(self):
        return self.value
        # return f"{romans[self.value]}" + ("+" if self.variation else "")


@dataclass(frozen=True)
class Cadenza:
    value: str

    def __str__(self):
        return f"c{self.value}"


@dataclass(frozen=True)
class End:
    variation: bool

    def __str__(self):
        return "end" + ("+" if self.variation else "")


@dataclass(frozen=True)
class Key:
    value: int
    max: int
    original: int

    def up(self, amt: int) -> Key:
        newval = self.value + amt
        if newval > self.max:
            return self.down(4)
        return replace(self, value=newval)

    def down(self, amt: int) -> Key:
        newval = self.value - amt
        if newval < 1:
            return replace(self, value=1)
        return replace(self, value=newval)

    def back(self) -> Key:
        if self.value < self.original:
            return self.up(1)
        return self.down(1)

    @property
    def idx(self):
        """The idx property."""
        return self.value


@dataclass(frozen=True)
class Fragment:
    phrase: Trio | Tuple[Phrase, Energy]
    part: Part
    # part: Cadenza | Part | End

    def __str__(self):
        if isinstance(self.phrase, Trio):
            return f"{self.phrase.value}_{self.part}"
        else:
            return f"{self.phrase[0].value}{self.phrase[1].value}_{self.part}"

    @classmethod
    def parse(cls, string: str) -> Fragment:
        string = string.strip()
        s = string[0]
        v, p = string[1:].split("_")
        try:
            phrase: Trio | Tuple[Phrase, Energy] = Trio(s)
        except ValueError:
            phrase = (Phrase(s), Energy.parse(v))
        return cls(phrase=phrase, part=Part(p))
        # part: Cadenza | Part | End
        # if p.startswith("c"):
        #     part = Cadenza(p[1:])
        # elif "end" in p:
        #     part = End(p.endswith("+"))
        # else:
        #     if p.endswith("+"):
        #         variation = True
        #         p = p[:-1]
        #     else:
        #         variation = False
        #     part = Part(romans.index(p), variation)
        # return cls(phrase=phrase, part=part)


@dataclass
class Connection:
    smooth: Sequence[Fragment]
    rough: Sequence[Fragment]
    end: Sequence[Fragment]


class Improviser(Thread):
    def __init__(
        self,
        activity: Activity,
        fragment_queue: Queue[Tuple[Fragment | Rest, Key]],
        segments: Mapping[Fragment, Tuple[float, float]],
        connections: Mapping[Fragment, Connection],
        start: Tuple[Fragment, Key],
    ):
        probs = []
        for f, v in connections.items():
            if f not in segments:
                probs.append(f"Fragment {f} missing in marker file")
            for f_ in chain(v.smooth, v.rough, v.end):
                if f_ not in segments:
                    probs.append(
                        f"Fragment {f_} missing in marker file (referenced from {f})"
                    )
        if probs:
            for p in probs:
                print(p)
            raise AssertionError("Fragments missing")
        self.fragments = list(segments)
        self.fragment_queue = fragment_queue
        self.activity = activity
        self.segments = segments
        self.connections = connections
        self.start_values = start
        self.end: List = []
        super().__init__()

    def run(self):
        for frag in improvise(
            self.start_values, self.activity, self.fragments, self.connections
        ):
            self.fragment_queue.put(frag)
            if self.end:
                return

    def stop(self, message: Any = None):
        self.end.append(message)


def improvise(
    start: Tuple[Fragment, Key],
    activity: Activity,
    fragments: List[Fragment],
    connections: Mapping[Fragment, Connection],
) -> Iterable[Tuple[Fragment | Rest, Key]]:
    p, k = start
    prev: Deque[Fragment] = deque(maxlen=4)
    yield (p, k)
    while True:
        prev.append(p)
        # phrs = p.phrase
        ps = get_next(p, connections, activity)
        if ps and random.random() > 0.01:
            p = random.choice(ps)
        else:
            if not ps:
                print(f"No candidates found for {p}")
            else:
                print(f"Wildcard")
            p = random.choice(fragments)
        if take_rest(p.phrase, prev[-1].phrase):
            r = random.random()
            duration = -math.log(r) * activity.rest_length() + 0.5
            yield (Rest(duration), k)
        if activity.go_back(random.random()):
            k = k.back()
        elif activity.go_up(random.random()) or p in prev:
            k = k.up(1)
        elif activity.go_down(random.random()):
            k = k.down(1)
        yield (p, k)


def take_rest(
    phrase: Tuple[Phrase, Energy] | Trio,
    previous: Tuple[Phrase, Energy] | Trio,
):
    if isinstance(phrase, Trio):
        return phrase != previous
    if isinstance(previous, Trio):
        return True
    return phrase[0] != previous[0]


def get_next(
    p: Fragment,
    connections: Mapping[Fragment, Connection],
    activity: Activity,
) -> Sequence[Fragment]:
    if p in connections:
        c = connections[p]
        # ends = [f for f in chain(c.smooth, c.rough) if isinstance(f.part, End)]
        if c.end and activity.exit(random.random()):
            print("Exiting cadenza...")
            return c.end
        if c.rough and activity.jump(random.random()):
            print("Picking rough transition...")
            return c.rough
        else:
            return c.smooth
    return []
