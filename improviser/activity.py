from __future__ import annotations

from collections import deque
from dataclasses import dataclass
from math import sqrt
from pathlib import Path
from threading import Thread
from time import sleep
from typing import TYPE_CHECKING, Any, Deque, List, Optional, Tuple

import numpy as np
from pythonosc.dispatcher import Dispatcher
from pythonosc.osc_server import ThreadingOSCUDPServer

from improviser.rules import activity as fuzzy_activity
from improviser.rules import average as fuzzy_average
from improviser.rules import (
    exit_rule,
    go_back_rule,
    go_down_rule,
    go_up_rule,
    jump_rule,
)
from improviser.rules import peak as fuzzy_peak
from improviser.rules import presence, rest_rule

if TYPE_CHECKING:
    from improviser.display import Display


class ActivityReader(Thread):
    def __init__(self, activity: Activity):
        self.activity = activity
        self.end: List = []
        self.mouse_pos = (0, 0)
        self.mouse_speed: Deque[float] = deque(maxlen=10)
        super().__init__()

    def run(self):
        import pyautogui

        while not self.end:
            mouse_pos = pyautogui.position()
            mouse_speed = self._dist(mouse_pos, self.mouse_pos)
            self.mouse_speed.append(mouse_speed)
            self.activity.value = np.mean(self.mouse_speed)
            self.activity.peak = np.max(self.mouse_speed)
            self.mouse_pos = mouse_pos
            sleep(0.050)

    def _dist(self, xs: Tuple[float, ...], ys: Tuple[float, ...]) -> float:
        return sqrt(sum((x - y) ** 2 for x, y in zip(xs, ys)))

    def stop(self, message: Any = None):
        self.end.append(message)


class ActivityRecording(Thread):
    def __init__(self, activity: Activity, recording: Path):
        self.activity = activity
        recording = np.load(recording)

        self.end: List = []
        self.mouse_speed: Deque[float] = deque(maxlen=10)
        self.oscthread = OSCThread(self.activity)
        super().__init__()

    def run(self):
        self.oscthread.start()

        while True:
            mouse_speed = 0
            while True:
                self.mouse_speed.append(mouse_speed)
                self.activity.value = np.mean(self.mouse_speed)
                self.activity.peak = np.max(self.mouse_speed)
                self.activity.presence += 0.050
                if self.end:
                    return
                sleep(0.050)

    def stop(self, message: Any = None):
        self.end.append(message)
        self.oscthread.server.server_close()


@dataclass
class OSCThread(Thread):
    port: int = 9001
    ip: str = "127.0.0.1"

    def __init__(self, activity):
        self.activity = activity
        self.dispatcher = Dispatcher()
        self.dispatcher.map("/sensor*", self.activity.visit)
        self.server = ThreadingOSCUDPServer(
            (self.ip, self.port), self.dispatcher
        )
        super().__init__()

    def run(self):
        self.server.serve_forever()

    def __hash__(self):
        return hash(id(self))

    def __eq__(self, other):
        return self is other


class Activity:
    def __init__(self, display: Display):
        self._value = 0.0
        self._average = 0.0
        self.peak = 0
        self.display = display
        self.presence = 0.0
        self.new_visit = False

    def visit(self, *_):
        self.presence = 0.0
        self.new_visit = True

    @property
    def value(self) -> float:
        return self._value

    @value.setter
    def value(self, value):
        self._value = value
        self._average = 0.98 * self._average + 0.02 * value
        self.display.update(self)

    @property
    def average(self) -> float:
        return self._average

    @property
    def values(self):
        return {
            fuzzy_activity: self.value,
            fuzzy_peak: self.peak,
            fuzzy_average: self.average,
        }

    def go_down(self, rand) -> bool:
        return rand < self.go_down_prob()

    def go_up(self, rand) -> bool:
        return rand < self.go_up_prob()

    def go_back(self, rand) -> bool:
        return rand < self.go_up_prob()

    def jump(self, rand) -> bool:
        return rand < self.jump_prob()

    def exit(self, rand) -> bool:
        return rand < self.exit_prob()

    def go_up_prob(self) -> float:
        return go_up_rule({fuzzy_average: self.average})

    def go_down_prob(self) -> float:
        return go_down_rule({fuzzy_peak: self.peak})

    def go_back_prob(self) -> float:
        return go_back_rule({fuzzy_average: self.average})

    def jump_prob(self) -> float:
        return jump_rule({fuzzy_peak: self.peak})

    def exit_prob(self) -> float:
        return exit_rule(
            {fuzzy_average: self.average, presence: self.presence}
        )

    def rest_length(self) -> float:
        return rest_rule({presence: self.presence})
