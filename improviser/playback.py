from __future__ import annotations

from pathlib import Path
from queue import Queue
from threading import Event, Thread
from typing import (
    TYPE_CHECKING,
    Any,
    Iterable,
    Iterator,
    List,
    Mapping,
    Optional,
    Tuple,
    TypeVar,
)

import numpy as np

from aplay import APlay
from improviser.statemachine import Fragment, Key, Rest

wav_folder = Path(__file__).parent.parent / "wav"

if TYPE_CHECKING:
    from improviser.activity import Activity

    T = TypeVar("T")


class Playback(Thread):
    def __init__(
        self,
        audio_file: np.ndarray,
        markers: Mapping[Fragment, Tuple[float, float]],
        fragments: Queue[Tuple[Fragment | Rest, Key]],
        activity: Activity,
        *,
        samplerate=24000,
        blocksize=0x100,
        channels=1,
        buffersize=2,
        overlap=0x200,
        record=False,
    ):
        self.event = Event()
        self.stream = APlay(rate=samplerate, channels=channels)
        self.queue: Queue[np.ndarray] = Queue(maxsize=buffersize)
        self.activity = activity
        self.blocksize = blocksize
        self.buffersize = buffersize
        self.samplerate = samplerate
        self.tape = AudioTape(
            audio_file,
            markers,
            fragments,
            activity,
            chunksize=self.blocksize,
            overlap=overlap,
            samplerate=samplerate,
        )
        self.end: List = []
        self.recording: Optional[List[np.ndarray]] = [] if record else None
        super().__init__()

    def run(self):
        with self.stream:
            for frame, key in self.tape:
                data = mix(frame)
                if self.recording is not None:
                    self.recording.append(data)
                self.stream.play(data)
                if self.end:
                    return

    def stop(self, message: Any = None):
        self.end.append(message)


def mix(data):
    return data[:, np.newaxis]


def iter_queue(queue: Queue[T]) -> Iterator[T]:
    while True:
        yield queue.get()


class AudioTape:
    def __init__(
        self,
        audio_file: np.ndarray,
        markers: Mapping[Fragment, Tuple[float, float]],
        fragments: Queue[Tuple[Fragment | Rest, Key]],
        activity: Activity,
        *,
        chunksize: int,
        overlap: int,
        samplerate: float,
    ):
        self.audio_file = audio_file
        self.markers = markers
        self.fragments = fragments
        self.activity = activity
        self.chunksize = chunksize
        self.samplerate = samplerate
        self.overlap = overlap

    def __iter__(self) -> Iterator[Tuple[np.ndarray, Key]]:
        fragment_ptr = 0
        fragments = self.overlap_fragments(iter_queue(self.fragments))
        fragment, key, name, f = next(fragments)
        while True:
            if name:
                print(f"{name} {key.value}")
            end = fragment.shape[0] - self.chunksize
            while fragment_ptr < end:
                yield fragment[
                    fragment_ptr : fragment_ptr + self.chunksize
                ], key
                fragment_ptr += self.chunksize
                print(f"  at {fragment_ptr}", end="\r")
                if self.activity.new_visit:
                    if isinstance(f, Rest):
                        fragment_ptr = end
                        print("Resuming for new visitor")
                    self.activity.new_visit = False
            nextone, key, name, f = next(fragments)
            rem = self.chunksize - (fragment.shape[0] - fragment_ptr)
            chunk = np.concatenate(
                [fragment[fragment_ptr:], nextone[:rem]], axis=0
            )
            yield (chunk, key)

            fragment_ptr = rem
            fragment = nextone

    def overlap_fragments(
        self, fragments: Iterable[Tuple[Fragment | Rest, Key]]
    ) -> Iterator[Tuple[np.ndarray, Key, str, Fragment | Rest]]:
        overlap_segment = np.zeros((self.overlap,))
        for f, key in fragments:
            # for fragment in map(self.load_segment, fragments):
            data, name, fragment = self.load_segment(f, key)
            if data.shape[0] > 2 * self.overlap:
                start, middle, end = np.split(
                    data, [self.overlap, -self.overlap]
                )
                yield (
                    self.transition(overlap_segment, start),
                    key,
                    "",
                    fragment,
                )
                yield (middle, key, name, fragment)
                overlap_segment = end
            else:
                print(f"Fragment {f} too short to overlap")

    def transition(self, from_segment, to_segment) -> np.ndarray:
        weight = np.linspace(0, 1, self.overlap)
        return (1 - weight) * from_segment + weight * to_segment

    def load_segment(
        self, segment: Fragment | Rest, key: Key
    ) -> Tuple[np.ndarray, str, Fragment | Rest]:
        if isinstance(segment, Rest):
            # print(f"Rest {segment.duration:.2f}s")
            return (
                np.zeros((int(segment.duration * self.samplerate),)),
                f"Rest {segment.duration:.2f}s",
                segment,
            )
        begin_f, end_f = self.markers[segment]
        begin = int(begin_f * self.samplerate)
        end = int(end_f * self.samplerate)
        # print(f"Segment {str(segment):<12s} ({begin:>8d} -- {end:>8d})")
        return (
            self.audio_file[
                begin - self.overlap : end + self.overlap,
                key.value - 1,
                ...,
            ],
            f"Segment {str(segment):<12s} ({begin / 24000:>8.3f} s -- {end / 24000:>8.3f} s)",
            segment,
        )
