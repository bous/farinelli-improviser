from __future__ import annotations

import tkinter as ttk
from dataclasses import dataclass, fields
from typing import TYPE_CHECKING

from pythonosc.udp_client import SimpleUDPClient

if TYPE_CHECKING:
    from improviser.activity import Activity


@dataclass
class Labels:
    ACTIVITY: ttk.Label
    activity_instant: ttk.Label
    activity_average: ttk.Label
    activity_peak: ttk.Label
    last_visitor: ttk.Label

    KEY_CHANGE_PROBABILITIES: ttk.Label
    go_back_probability: ttk.Label
    go_up_probability: ttk.Label
    go_down_probability: ttk.Label

    KADENZA_PROBABILITIES: ttk.Label
    jump: ttk.Label
    exit: ttk.Label
    rest_duration: ttk.Label


def make_rows(frame):
    for row, field in enumerate(fields(Labels)):
        label = ttk.Label(frame, anchor="w", text=field.name.replace("_", " "))
        label.grid(column=0, row=row, sticky=ttk.W)
        value = ttk.Label(frame, text="")
        value.grid(column=1, row=row)
        yield value


class Display:
    def __init__(self):
        self.root = ttk.Tk()
        self.frame = ttk.Frame(self.root)
        self.frame.grid()

        self.labels = Labels(*make_rows(self.frame))

        oscclient = SimpleUDPClient("127.0.0.1", 9001)
        ttk.Button(
            self.frame,
            text="Visit",
            command=lambda *_: oscclient.send_message("/sensor 1", 1),
        ).grid(column=1, row=100)
        ttk.Button(self.frame, text="Quit", command=self.root.destroy).grid(
            column=1, row=200
        )

    def update(self, activity: Activity):
        self.labels.activity_instant.config(text=f"{activity.value:4.0f}")
        self.labels.activity_average.config(text=f"{activity.average:4.0f}")
        self.labels.activity_peak.config(text=f"{activity.peak:4.0f}")
        self.labels.last_visitor.config(text=f"{activity.presence:2.1f} s")

        back = activity.go_back_prob()
        back_text = f"{100 * back:2.1f} %"
        self.labels.go_back_probability.config(text=back_text)

        down = activity.go_down_prob()
        down_text = f"{100 * down:2.1f} %"
        self.labels.go_down_probability.config(text=down_text)

        up = activity.go_up_prob()
        up_text = f"{100 * up:2.1f} %"
        self.labels.go_up_probability.config(text=up_text)

        r = activity.rest_length()
        self.labels.rest_duration.config(text=f"{r + 0.5:2.1f} s")

        j = activity.jump_prob()
        e = activity.exit_prob()
        self.labels.jump.config(text=f"{100 * j:2.1f} %")
        self.labels.exit.config(text=f"{100 * e:2.1f} %")

    def run(self):
        self.root.mainloop()
