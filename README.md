# Farinelli Improviser

Improvises a new version of Giacomelli's 17 century hit song *Quell'usignolo*.

## Note on the public repository

Since the audio files are not free from copyright,
I cannot distribute the necessary audio files in this repo.
Therefore, you will not be able to run the improviser out of the box.
If you want to try out the improviser using the Quell'usignolo song,
please contact me at [farinelli@fnab.xyz](mailto:farinelli@fnab.xyz)
to request a copy of the audio files.
Otherwise, feel free to generate improvisations over your own recordings
by placing them into the `audio/` folder and adjusting the files
`data/connections.scv` and `data/segments.txt`.

## Sample

Below a short audio sample of what the improviser does

<iframe width="100%" height="300" scrolling="no" frameborder="no" allow="autoplay" src="https://w.soundcloud.com/player/?url=https%3A//api.soundcloud.com/tracks/1684803579&color=%23ff5500&auto_play=false&hide_related=false&show_comments=true&show_user=true&show_reposts=false&show_teaser=true&visual=true"></iframe><div style="font-size: 10px; color: #cccccc;line-break: anywhere;word-break: normal;overflow: hidden;white-space: nowrap;text-overflow: ellipsis; font-family: Interstate,Lucida Grande,Lucida Sans Unicode,Lucida Sans,Garuda,Verdana,Tahoma,sans-serif;font-weight: 100;"><a href="https://soundcloud.com/frederik-bous" title="Frederik Bous" target="_blank" style="color: #cccccc; text-decoration: none;">Frederik Bous</a> · <a href="https://soundcloud.com/frederik-bous/farinelli-improviser" title="Farinelli Improviser" target="_blank" style="color: #cccccc; text-decoration: none;">Farinelli Improviser</a></div>

## Installation

1. Make sure you have `conda` installed.
   To check whether `conda` is installed
   you can run `conda --version` in a terminal.
   If you get something like `bash: Unknown command conda` as a response
   you will need to [install conda](https://docs.anaconda.com/free/anaconda/install/index.html).
2. In a terminal move to the folder containing this README.
3. On Linux/Mac run

   ```bash
   install-conda.sh
   ```

   For windows use the batch script instead

   ```bash
   install-conda.bat
   ```

   This will create a conda environment with all the necessary dependencies.

## Launching

### Linux / Mac

In a terminal (after moving to the folder containing the readme) run
```bash
./run-conda.sh
```

### Windows

In a terminal (after moving to the folder containing the readme) run
```bash
./run-conda.bat
```

( Make sure you are in the correct folder. )
